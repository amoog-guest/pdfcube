pdfcube (0.0.5-3) unstable; urgency=medium

  * debian/control:
    - Switch to salsa uri

 -- Andreas Moog <andreas.moog@warperbbs.de>  Wed, 30 May 2018 20:25:43 +0200

pdfcube (0.0.5-2) unstable; urgency=medium

  * debian/control:
    - Change maintainer email address
    - Update standards version to 3.9.5, no changes needed
  * debian/rules:
    - Pass --with-boost-libdir to configure script. The auto-detection
      got broken with the multiarched boost libraries. (Closes: #738429)

 -- Andreas Moog <andreas.moog@warperbbs.de>  Sun, 09 Feb 2014 18:25:49 +0100

pdfcube (0.0.5-1) unstable; urgency=low

  * New upstream release
  * debian/control: Standards version is 3.9.4
  * debian/patches: Removed all patches, integrated upstream
  * debian/copyright: Updated
  * debian/patches/0001-hyphen.patch: New patch:
    - Don't use hyphen as minus in manpage
  * debian/rules: Use dh_installman to install manpage to correct location

 -- Andreas Moog <amoog@ubuntu.com>  Wed, 17 Jul 2013 15:33:06 +0200

pdfcube (0.0.4-2) unstable; urgency=low

  * debian/source/format: Use 3.0 (quilt) source format
  * debian/rules: Use DEB_LDFLAGS_MAINT_APPEND instead of LDFLAGS to ensure
    that all hardening flags are passed correctly. (Closes: #662235)
    Thanks Simon Ruderich <simon AT ruderich.org>
  * debian/patches/array_bounds.patch: The top_color array is defined as
    containing 4 elements, yet the fifth element was accessed. 
    (Closes: #662240)
    Thanks Simon Ruderich <simon AT ruderich.org>
  * debian/control, d/rules, d/pdfcube.install, d/pdfcube.manpages:
    - add pdfcube-dbg package

 -- Andreas Moog <amoog@ubuntu.com>  Mon, 05 Mar 2012 21:04:59 +0100

pdfcube (0.0.4-1) unstable; urgency=low

  * New Maintainer (Closes: #651724)
  * New upstream release:
    - builds with libpoppler 0.18
    - include manpage (Closes: #464830)
    - handle command line options (Closes: #507880)
  * debian/{compat,control,rules}:
    - use dh sequencer
    - bump debhelper compat level to 9
    - export buildflags from dpkg-buildflags
    - override dh_installman cause upstream doesn't install it
  * debian/control:
    - wrap and sort build-depends
    - drop freeglut3-dev build-depends, not needed anymore
    - drop libpoppler-dev build-depends, pdfcube only needs poppler-glib
      Thanks Pino Toscano <pino@debian.org> (Closes: #659531)
    - add libboost-dev and libboost-program-options-dev build-depends
    - fix spelling in description (Closes: #425263)
      Thanks Peter Eisentraut <peter_e@gmx.net>
    - add Vcs-Git and Vcs-Browser fields
    - move homepage to Homepage field (Closes: #615438)
    - bump standards version to 3.9.3
  * debian/copyright:
    - convert to DEP5 copyright format
  * debian/dirs:
    - removed, it's not needed
  * debian/docs:
    - don't install empty NEWS file
  * debian/pdfcube.1[.txt]:
    - removed, included upstream
  * debian/rules:
    - allow parallel building
  * debian/watch - added

 -- Andreas Moog <amoog@ubuntu.com>  Thu, 15 Dec 2011 10:19:12 +0100

pdfcube (0.0.2-3) unstable; urgency=low

  * Updated manpage 

 -- Alexander Wirt <formorer@debian.org>  Thu, 15 Feb 2007 06:56:45 +0100

pdfcube (0.0.2-2) unstable; urgency=low

  * Fix manpage (Closes: #409155)

 -- Alexander Wirt <formorer@debian.org>  Wed, 31 Jan 2007 10:35:43 +0100

pdfcube (0.0.2-1) unstable; urgency=low

  * Initial release (Closes: #403862)

 -- Alexander Wirt <formorer@debian.org>  Sun, 28 Jan 2007 18:54:01 +0100

